# encoding: utf-8

##########################
####    Categories   #####
##########################

c1 = Category.new
c1.name = "Programiranje"
c1.parent_id = 0
c1.save

c2 = Category.new
c2.name = "Dizajn"
c2.parent_id = 0
c2.save

c3 = Category.new
c3.name = "Fotografija"
c3.parent_id = 0
c3.save

c4 = Category.new
c4.name = "Arhitektura"
c4.parent_id = 0
c4.save

c5 = Category.new
c5.name = "Sinematografija"
c5.parent_id = 0
c5.save

c6 = Category.new
c6.name = "Umetnost"
c6.parent_id = 0
c6.save

c7 = Category.new
c7.name = "Pisanje"
c7.parent_id = 0
c7.save

c8 = Category.new
c8.name = "Muzika"
c8.parent_id = 0
c8.save

c9 = Category.new
c9.name = "Animacija"
c9.parent_id = 0
c9.save


# Subcategories
# Programiranje

c11 = Category.new
c11.name = "HTML/CSS/JS"
c11.parent_id = 1
c11.save

c12 = Category.new
c12.name = "JEE"
c12.parent_id = 1
c12.save

c13 = Category.new
c13.name = "Java/Android"
c13.parent_id = 1
c13.save


c14 = Category.new
c14.name = "iOS/OSX"
c14.parent_id = 1
c14.save


c15 = Category.new
c15.name = "PHP"
c15.parent_id = 1
c15.save

c16 = Category.new
c16.name = "Ruby"
c16.parent_id = 1
c16.save

c17 = Category.new
c17.name = "Python"
c17.parent_id = 1
c17.save

c18 = Category.new
c18.name = ".NET"
c18.parent_id = 1
c18.save

# Dizajn
c21 = Category.new
c21.name = "Grafički"
c21.parent_id = 2
c21.save

c22 = Category.new
c22.name = "Interfejsi"
c22.parent_id = 2
c22.save

c23 = Category.new
c23.name = "Dizajn interakcija"
c23.parent_id = 2
c23.save

c24 = Category.new
c24.name = "Korisničko iskustvo"
c24.parent_id = 2
c24.save

c25 = Category.new
c25.name = "Industrijski"
c25.parent_id = 2
c25.save

c26 = Category.new
c26.name = "Digitalni proizvodi"
c26.parent_id = 2
c26.save

c27 = Category.new
c27.name = "Brend"
c27.parent_id = 2
c27.save

c28 = Category.new
c28.name = "Modni"
c28.parent_id = 2
c28.save

# Fotografija
c31 = Category.new
c31.name = "Crno bela"
c31.parent_id = 3
c31.save

c32 = Category.new
c32.name = "Modna"
c32.parent_id = 3
c32.save

c33 = Category.new
c33.name = "Dokumentarna"
c33.parent_id = 3
c33.save

c34 = Category.new
c34.name = "Umetnička"
c34.parent_id = 3
c34.save

c35 = Category.new
c35.name = "Podvodna"
c35.parent_id = 3
c35.save

c36 = Category.new
c36.name = "Portreti"
c36.parent_id = 3
c36.save

# Arhitektura
c41 = Category.new
c41.name = "Pejzažna"
c41.parent_id = 4
c41.save

c42 = Category.new
c42.name = "Enterijer"
c42.parent_id = 4
c42.save

c43 = Category.new
c43.name = "Eksterijer"
c43.parent_id = 4
c43.save

# Sinematografija
c51 = Category.new
c51.name = "Filmovi"
c51.parent_id = 5
c51.save

c52 = Category.new
c52.name = "Spotovi"
c52.parent_id = 5
c52.save

c53 = Category.new
c53.name = "Spacijalni efekti"
c53.parent_id = 5
c53.save

c54 = Category.new
c54.name = "Post produkcija"
c54.parent_id = 5
c54.save

# Umetnost
c61 = Category.new
c61.name = "Slikanje"
c61.parent_id = 6
c61.save

c62 = Category.new
c62.name = "Vajanje"
c62.parent_id = 6
c62.save

c63 = Category.new
c63.name = "Crtež"
c63.parent_id = 6
c63.save

c64 = Category.new
c64.name = "Ilustracija"
c64.parent_id = 6
c64.save

c65 = Category.new
c65.name = "Kolaž"
c65.parent_id = 6
c65.save

c66 = Category.new
c66.name = "Performans"
c66.parent_id = 6
c66.save

c67 = Category.new
c67.name = "Nakit"
c67.parent_id = 6
c67.save

# Pisanje
c71 = Category.new
c71.name = "Proza"
c71.parent_id = 7
c71.save

c72 = Category.new
c72.name = "Poezija"
c72.parent_id = 7
c72.save

c73 = Category.new
c73.name = "Kopivrajting"
c73.parent_id = 7
c73.save

c74 = Category.new
c74.name = "Novinarstvo"
c74.parent_id = 7
c74.save

c75 = Category.new
c75.name = "Blog"
c75.parent_id = 7
c75.save

c76 = Category.new
c76.name = "Scenario"
c76.parent_id = 7
c76.save

# Muzika
c81 = Category.new
c81.name = "Pevanje"
c81.parent_id = 8
c81.save

c82 = Category.new
c82.name = "Produkcija"
c82.parent_id = 8
c82.save

c83 = Category.new
c83.name = "DJ-ing"
c83.parent_id = 8
c83.save

c84 = Category.new
c84.name = "Bubnjevi"
c84.parent_id = 8
c84.save

c85 = Category.new
c85.name = "Gitara"
c85.parent_id = 8
c85.save

c86 = Category.new
c86.name = "Bas-gitara"
c86.parent_id = 8
c86.save

c87 = Category.new
c87.name = "Klavir"
c87.parent_id = 8
c87.save

# Animacija
c91 = Category.new
c91.name = "Motion Graphics"
c91.parent_id = 9
c91.save

c92 = Category.new
c92.name = "2D"
c92.parent_id = 9
c92.save

c93 = Category.new
c93.name = "3D"
c93.parent_id = 9
c93.save

c94 = Category.new
c94.name = "Stop animacija"
c94.parent_id = 9
c94.save

