class CategoriesVastromen < ActiveRecord::Migration
  def up
  	  create_table :categories_vastromen, :id => false do |t|
      t.integer :category_id
      t.integer :vastromen_id
    end
  end

  def down
  	drop_table :categories_vastromen
  end
end
