class CreateVastromen < ActiveRecord::Migration
  def change
    create_table :vastromen do |t|
    	t.string :name
    	t.string :surname
    	t.string :url
    	t.string :email
      t.integer :city_id
      t.boolean :active, :default => false
    	t.float	 :vastrodex, :default => 0
      	t.timestamps
    end
  end
end
