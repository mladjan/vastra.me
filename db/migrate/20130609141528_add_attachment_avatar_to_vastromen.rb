class AddAttachmentAvatarToVastromen < ActiveRecord::Migration
  def self.up
    change_table :vastromen do |t|
      t.attachment :avatar
    end
  end

  def self.down
    drop_attached_file :vastromen, :avatar
  end
end
