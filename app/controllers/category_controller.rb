class CategoryController < ApplicationController

	def index
		@users = Vastromen.limit(50)
	end

	def show
		@category = Category.where("name = ?", params[:id]).first
	end
end
