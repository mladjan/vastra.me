class VastromenController < ApplicationController
  def index
  end

  def new
  	@vastromen = Vastromen.new

  	@programiranje = Category.where("parent_id = ?", 1)
  	@dizajn = Category.where("parent_id = ?", 2)
  	@fotografija = Category.where("parent_id = ?", 3)
  	@arhitektura = Category.where("parent_id = ?", 4)
  	@sinematografija = Category.where("parent_id = ?", 5)
  	@umetnost = Category.where("parent_id = ?", 6)
  	@pisanje = Category.where("parent_id = ?", 7)
  	@muzika = Category.where("parent_id = ?", 8)
  	@animacija = Category.where("parent_id = ?", 9)

  end

  def create
	@vm = Vastromen.create( params[:vastromen] )
	logger.debug "Problem attributes hash: #{@vm.attributes.inspect}"

    respond_to do |format|
      if @vm.save
      	format.html { render action: "thankyou" }
      else
        format.html { render action: "new" }
        format.json { render json: @upload.errors, status: :unprocessable_entity }
      end
    end
  end
end
