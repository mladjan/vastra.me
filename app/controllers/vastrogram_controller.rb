class VastrogramController < ApplicationController
  def index
  	@allCategories = Category.all
	@categoriesWithVastomens = []

  	@allCategories.each do |i|
  		if(i.vastromens.count > 0)
  			@categoriesWithVastomens << lambda { i }
  		end
  	end

  end

end
